
// ## Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.


// У контексті розробки Javascript, AJAX дуже корисний, оскільки він дозволяє взаємодіяти
// з сервером без необхідності повного оновлення сторінки. Це дає можливість створювати
// більш динамічні та інтерактивні веб - сторінки, які можуть змінюватися без втрати даних,
//   що були введені користувачем.
// Але AJAX маэ потенційні недоліки, на які треба звертати увагу:
// Безпека: AJAX може бути вразливим до атак типу Cross - Site Scripting(XSS) або Cross - Site Request Forgery(CSRF),
// які можуть призвести до витоку конфіденційної інформації або несанкціонованого доступу до сервера.

// Перевантаження сервера: якщо AJAX запити робляться занадто часто або запитуються великі обсяги даних,
// це може перевантажити сервер і спричинити проблеми з продуктивністю веб - сайту.




 



   function loadData(url) {
    return fetch(url)
    .then((response) => {
      if (!response.ok) throw new Error("HTTP error");
      return response.json();
    })
    .catch((error) => console.error("Fetch problems", error));
}
  

   const films = (obj) => `<li> <span><b> Episode №${obj.episodeId}</b></span> 
                          <h3 data-number="${obj.episodeId}"><b>${obj.name}</b>
                          </h3><p>${obj.openingCrawl}</p><hr></li> `;

   loadData("https://ajax.test-danit.com/api/swapi/films")
   .then((data) => showData(data, document.getElementById("user-list"), films));

  function showData(arr, insertIn, callback) {
  arr.forEach((item) => {
    insertIn.insertAdjacentHTML("afterbegin", callback(item));
  
    let requests = item.characters.map((chc) =>
      loadData(chc));
    
    Promise.all(requests)
      .then(responses => {
        for (let response of responses) {
         document.querySelectorAll(`[data-number='${item.episodeId}']`).forEach((el) =>
           el.insertAdjacentHTML("afterend", `<h5>Character: ${response.name}</h5>`));
  
        }
    
      });
  });
}

